# Aidge backend OpenCV library

You can find in this folder the library that implements :
- Computer vision databases 
- The OpenCV tensor, operators and stimulis

So far be sure to have the correct requirements to use this library
[TOC]

## Installation
### Dependencies
- `GCC`
- `OpenCV` >= 3.0
- `Make`
- `CMake`
- `Python` (optional, if you have no intend to use this library in python with pybind)

#### aidge dependencies
 - `aidge_core`
 - `aidge_backend_cpu`.

### Pip installation
``` bash
pip install . -v
```
> **TIPS :** Use environment variables to change compilation options :
> - `AIDGE_INSTALL` : to set the installation folder. Defaults to /usr/local/lib. :warning: This path must be identical to aidge_core install path.
> - `AIDGE_PYTHON_BUILD_TYPE` : to set the compilation mode to **Debug** or **Release** 
> - `AIDGE_BUILD_GEN` : to set the build backend with 

