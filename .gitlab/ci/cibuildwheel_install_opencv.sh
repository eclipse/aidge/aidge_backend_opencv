git clone https://github.com/opencv/opencv.git
cd opencv
git checkout 4.9.0
mkdir build
cd build 
cmake .. \
  -DENABLE_SOLUTION_FOLDERS=OFF\
  -DBUILD_TESTS=OFF \
  -DBUILD_opencv_ml=ON \
  -DBUILD_opencv_java_bindings_generator=OFF \
  -DBUILD_opencv_js=OFF \
  -DBUILD_opencv_js_bindings_generator=OFF \
  -DBUILD_opencv_objc_bindings_generator=OFF \
  -DBUILD_opencv_python_bindings_generator=OFF \
  -DBUILD_opencv_apps=OFF \
  -DBUILD_opencv_calib3d=OFF \
  -DBUILD_opencv_videoio=OFF \
  -DBUILD_opencv_highgui=OFF \
  -DBUILD_opencv_gapi=OFF \

  -DBUI
make -j8
make install
