#include <pybind11/pybind11.h>
// Need to call this header to register tensorImpl when initializing opencv python module
#include "aidge/backend/opencv.hpp"

namespace py = pybind11;

namespace Aidge {

void init_MNIST(py::module&);
void init_OpencvVersionInfo(py::module&);

PYBIND11_MODULE(aidge_backend_opencv, m) {
    init_MNIST(m);
    init_OpencvVersionInfo(m);
}
}
