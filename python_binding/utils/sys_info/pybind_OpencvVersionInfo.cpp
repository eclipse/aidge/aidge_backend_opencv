#include <pybind11/pybind11.h>
#include "aidge/utils/sys_info/OpencvVersionInfo.hpp"

namespace py = pybind11;
namespace Aidge {
void init_OpencvVersionInfo(py::module& m){
    m.def("show_version", &showBackendOpencvVersion);
    m.def("get_project_version", &getBackendOpencvProjectVersion);
    m.def("get_git_hash", &getBackendOpencvGitHash);
}
}
