#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "aidge/backend/opencv/database/MNIST.hpp"

namespace py = pybind11;
namespace Aidge {

void init_MNIST(py::module& m){

    py::class_<MNIST, std::shared_ptr<MNIST>, Database>(m, "MNIST")
          .def(py::init<const std::string&, bool, bool>(), py::arg("dataPath"), py::arg("train"), py::arg("load_data_in_memory")=false)
          .def("get_item", &MNIST::getItem, py::arg("index"),
          R"mydelimiter(
          Return samples of each data modality for the given index.

          :param index: Database index corresponding to one item
          :type index: int
          )mydelimiter")
          
          .def("get_len", &MNIST::getLen,
          R"mydelimiter(
          Return the number of items in the database.

          )mydelimiter")

          .def("get_nb_modalities", &MNIST::getNbModalities,
          R"mydelimiter(
          Return the number of modalities in one item of the database.
          
          )mydelimiter");
    
}
}
