/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <opencv2/core.hpp>  // cv::Mat, cv::split
#include <cstddef>
#include <cstdint>
#include <cstring>   // std::memcpy, std::strcmp
#include <stdexcept> // std::runtime_error
#include <memory>
#include <vector>

#include "aidge/backend/opencv/utils/Utils.hpp"
#include "aidge/backend/opencv/data/DataUtils.hpp"  // detail::CvtoAidge
#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/opencv/data/TensorImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"                      // DimSize_t

static Aidge::DataType CVtoAidge(const int matDepth) {
    Aidge::DataType res;
    switch (matDepth) {
        case CV_8U:
            res = Aidge::DataType::UInt8;
            break;
        case CV_8S:
            res = Aidge::DataType::Int8;
            break;
        case CV_16U:
            res = Aidge::DataType::UInt16;
            break;
        case CV_16S:
            res = Aidge::DataType::Int16;
            break;
        case CV_16F:
            res = Aidge::DataType::Float16;
            break;
        case CV_32S:
            res = Aidge::DataType::Int32;
            break;
        case CV_32F:
            res = Aidge::DataType::Float32;
            break;
        case CV_64F:
            res = Aidge::DataType::Float64;
            break;
        default:
            throw std::runtime_error(
                "Cannot convert cv::Mat to Tensor: incompatible types.");
        }
    return res;
}

std::shared_ptr<Aidge::Tensor> Aidge::tensorOpencv(cv::Mat mat) {
    // Get Mat dims
    const std::vector<DimSize_t> matDims = std::vector<DimSize_t>({static_cast<DimSize_t>(mat.channels()),
                                                            static_cast<DimSize_t>(mat.rows),
                                                            static_cast<DimSize_t>(mat.cols)});
    // Get the correct Data Type
    Aidge::DataType type;
    type = CVtoAidge(mat.depth());
    // Create tensor from the dims of the Cv::Mat
    std::shared_ptr<Tensor> tensor = std::make_shared<Tensor>(matDims);
    // Set beackend opencv
    tensor->setBackend("opencv");
    // Set datatype
    tensor->setDataType(type);

    // Cast the tensorImpl to access setCvMat function
    TensorImpl_opencv_* tImpl_opencv = dynamic_cast<TensorImpl_opencv_*>(tensor->getImpl().get());
    tImpl_opencv->setCvMat(mat);
    return tensor;
}


template <class CV_T>
void Aidge::convert(const cv::Mat& mat, void* data, std::size_t offset)
{
    if (mat.isContinuous())
        std::memcpy(reinterpret_cast<void*>(reinterpret_cast<CV_T*>(data) + offset), mat.ptr<CV_T>(), sizeof(CV_T)*(mat.cols*mat.rows));
    else {
        throw std::runtime_error(
                "Poui pwoup convert not support if matrix not contiguous");
    }

}


std::shared_ptr<Aidge::Tensor> Aidge::convertCpu(std::shared_ptr<Aidge::Tensor> tensorOpencv){
    // Assert the tensorOpencv is backend Opencv
    AIDGE_ASSERT(tensorOpencv->getImpl()->backend() == "opencv", "Cannot convert tensor backend from opencv to cpu : tensor is not backend opencv.");

    //  Create a tensor backend cpu from the dimensions of the tensor backend opencv
    std::shared_ptr<Aidge::Tensor> tensorCpu = std::make_shared<Aidge::Tensor>(tensorOpencv->dims());

    // Get the cv::Mat from the tensor backend Opencv
    Aidge::TensorImpl_opencv_* tImplOpencv = dynamic_cast<Aidge::TensorImpl_opencv_*>(tensorOpencv->getImpl().get());
    cv::Mat dataOpencv = tImplOpencv->data();

    // Convert the cv::Mat into a vector of cv::Mat (vector of channels)
    std::vector<cv::Mat> channels;
    cv::split(dataOpencv, channels);

    // set the datatype of the cpu tensor
    tensorCpu->setDataType(CVtoAidge(channels[0].depth()));

    // Set backend cpu
    tensorCpu->setBackend("cpu");

    // Convert & copy the cv::Mat into the tensor using the rawPtr of tensor cpu
    std::size_t count = 0;
    for (std::vector<cv::Mat>::const_iterator itChannel = channels.cbegin();
        itChannel != channels.cend();
        ++itChannel)
    {
        switch ((*itChannel).depth()) {
        case CV_8U:
            convert<unsigned char>(*itChannel, tensorCpu->getImpl()->rawPtr(), count*static_cast<std::size_t>((*itChannel).rows*(*itChannel).cols));
            break;
        case CV_8S:
            convert<char>(*itChannel, tensorCpu->getImpl()->rawPtr(), count*static_cast<std::size_t>((*itChannel).rows*(*itChannel).cols));
            break;
        case CV_16U:
            convert<unsigned short>(*itChannel, tensorCpu->getImpl()->rawPtr(), count*static_cast<std::size_t>((*itChannel).rows*(*itChannel).cols));
            break;
        case CV_16S:
            convert<short>(*itChannel, tensorCpu->getImpl()->rawPtr(), count*static_cast<std::size_t>((*itChannel).rows*(*itChannel).cols));
            break;
        case CV_32S:
            convert<int>(*itChannel, tensorCpu->getImpl()->rawPtr(), count*static_cast<std::size_t>((*itChannel).rows*(*itChannel).cols));
            break;
        case CV_32F:
            convert<float>(*itChannel, tensorCpu->getImpl()->rawPtr(), count*static_cast<std::size_t>((*itChannel).rows*(*itChannel).cols));
            break;
        case CV_64F:
            convert<double>(*itChannel, tensorCpu->getImpl()->rawPtr(), count*static_cast<std::size_t>((*itChannel).rows*(*itChannel).cols));
            break;
        default:
            throw std::runtime_error(
                "Cannot convert cv::Mat to Tensor: incompatible types.");
        }
        ++count;
    }
    return tensorCpu;
}
