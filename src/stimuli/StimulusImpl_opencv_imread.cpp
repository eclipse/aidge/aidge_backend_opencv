/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/opencv/stimuli/StimulusImpl_opencv_imread.hpp"

#include <memory>
#include <stdexcept>
#include <string>

#include "opencv2/core.hpp"

#include "aidge/data/Tensor.hpp"
#include "aidge/backend/opencv/utils/Utils.hpp"

Aidge::StimulusImpl_opencv_imread::~StimulusImpl_opencv_imread() noexcept = default;

std::shared_ptr<Aidge::Tensor> Aidge::StimulusImpl_opencv_imread::load() const {
    cv::Mat cvImg = cv::imread(mDataPath, mReadMode);
    if (cvImg.empty()) {
        throw std::runtime_error("Could not open images file: " + mDataPath);
    }

    return tensorOpencv(cvImg);
}
