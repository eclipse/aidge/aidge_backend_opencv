/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/opencv/database/MNIST.hpp"

#include <cstdint>
#include <fstream>
#include <iomanip>
#include <tuple>

#include "opencv2/core.hpp"
#include <opencv2/opencv.hpp>
#include "aidge/backend/opencv/utils/Utils.hpp"


Aidge::MNIST::~MNIST() noexcept = default;

void Aidge::MNIST::uncompress(const std::string& dataPath,
                            const std::string& labelPath)
{
    // Images
    std::ifstream images(dataPath.c_str(), std::fstream::binary);

    if (!images.good())
        throw std::runtime_error("Could not open images file: " + dataPath);

    MagicNumber magicNumber;
    std::uint32_t nbImages;
    std::uint32_t nbRows;
    std::uint32_t nbColumns;

    images.read(reinterpret_cast<char*>(&magicNumber.value),
                sizeof(magicNumber));
    images.read(reinterpret_cast<char*>(&nbImages), sizeof(nbImages));
    images.read(reinterpret_cast<char*>(&nbRows), sizeof(nbRows));
    images.read(reinterpret_cast<char*>(&nbColumns), sizeof(nbColumns));

    if (!Aidge::isBigEndian()) {
        Aidge::swapEndian(magicNumber.value);
        Aidge::swapEndian(nbImages);
        Aidge::swapEndian(nbRows);
        Aidge::swapEndian(nbColumns);
    }

    // if (magicNumber.byte[3] != 0 || magicNumber.byte[2] != 0
    //     || magicNumber.byte[1] != Unsigned || magicNumber.byte[0] != 3) {
    if (magicNumber.value != 0x00000803) { // 0, 0, unisgned, 3
        throw std::runtime_error("Wrong file format for images file: "
                                 + dataPath);
    }

    // Labels
    std::ifstream labels(labelPath.c_str(), std::fstream::binary);

    if (!labels.good())
        throw std::runtime_error("Could not open labels file: " + labelPath);

    MagicNumber magicNumberLabels;
    std::uint32_t nbItemsLabels;

    labels.read(reinterpret_cast<char*>(&magicNumberLabels.value),
                sizeof(magicNumberLabels));
    labels.read(reinterpret_cast<char*>(&nbItemsLabels), sizeof(nbItemsLabels));

    if (!Aidge::isBigEndian()) {
        Aidge::swapEndian(magicNumberLabels);
        Aidge::swapEndian(nbItemsLabels);
    }

    // if (magicNumberLabels.byte[3] != 0 || magicNumberLabels.byte[2] != 0
    //     || magicNumberLabels.byte[1] != Unsigned
    //     || magicNumberLabels.byte[0] != 1) {
    if (magicNumberLabels.value != 0x00000801) { // 0, 0, unsigned, 1
        throw std::runtime_error("Wrong file format for labels file: "
                                 + labelPath);
    }

    if (nbImages != nbItemsLabels)
        throw std::runtime_error(
            "The number of images and the number of labels does not match.");

    // For each image...
    for (std::uint32_t i = 0; i < nbImages; ++i) {
        std::uint8_t buff;

        std::ostringstream nameStr;
        nameStr << dataPath << "[" << std::setfill('0') << std::setw(5) << i
                << "].pgm";

        // ... generate the stimuli
        if (!std::ifstream(nameStr.str()).good()) {
            cv::Mat frame(cv::Size(nbColumns, nbRows), CV_8UC1);

            for (std::uint32_t y = 0; y < nbRows; ++y) {
                for (std::uint32_t x = 0; x < nbColumns; ++x) {
                    images.read(reinterpret_cast<char*>(&buff), sizeof(buff));
                    frame.at<std::uint8_t>(y, x) = buff;
                }
            }

            if (!cv::imwrite(nameStr.str(), frame))
                throw std::runtime_error("Unable to write image: "
                                         + nameStr.str());
        } else {
            // Skip image data (to grab labels only)
            images.seekg(nbColumns * nbRows, images.cur);
        }

        // Create the stimuli of the image
        Aidge::Stimulus StimulusImg(nameStr.str(), mLoadDataInMemory);
        StimulusImg.setBackend("opencv");

        // Create the stimulus of the corresponding label by filing integer to the stimulus directly
        labels.read(reinterpret_cast<char*>(&buff), sizeof(buff));
        const std::int32_t label = std::move(static_cast<std::int32_t>(buff));

        std::shared_ptr<Tensor> lbl = std::make_shared<Tensor>(Array1D<int, 1>{label});
        Aidge::Stimulus StimulusLabel(lbl);

        // Push back the corresponding image & label in the vector
        mStimuli.push_back(std::make_tuple(StimulusImg, StimulusLabel));
    }

    if (images.eof())
        throw std::runtime_error(
            "End-of-file reached prematurely in data file: " + dataPath);
    else if (!images.good())
        throw std::runtime_error("Error while reading data file: " + dataPath);
    else if (images.get() != std::fstream::traits_type::eof())
        throw std::runtime_error("Data file size larger than expected: "
                                 + dataPath);

    if (labels.eof())
        throw std::runtime_error(
            "End-of-file reached prematurely in data file: " + labelPath);
    else if (!labels.good())
        throw std::runtime_error("Error while reading data file: " + labelPath);
    else if (labels.get() != std::fstream::traits_type::eof())
        throw std::runtime_error("Data file size larger than expected: "
                                 + labelPath);
}


std::vector<std::shared_ptr<Aidge::Tensor>> Aidge::MNIST::getItem(const std::size_t index) const {
    std::vector<std::shared_ptr<Tensor>> item;
    // Load the digit tensor
    // TODO : Currently converts the tensor Opencv but this operation will be carried by a convert operator in the preprocessing graph
    item.push_back(Aidge::convertCpu((std::get<0>(mStimuli.at(index))).load()));
    // item.push_back((std::get<0>(mStimuli.at(index))).load());
    // Load the label tensor
    item.push_back((std::get<1>(mStimuli.at(index))).load());

    return item;
}