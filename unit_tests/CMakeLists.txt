# Catch2 configuration
set(CATCH2_MIN_VERSION 3.3.0)

# Try to find system installed Catch2
find_package(Catch2 ${CATCH2_MIN_VERSION} QUIET)

if(NOT Catch2_FOUND)
    message(STATUS "Catch2 not found in system, retrieving from git")
    Include(FetchContent)

    FetchContent_Declare(
      Catch2
      GIT_REPOSITORY https://github.com/catchorg/Catch2.git
      GIT_TAG        devel # or a later release
    )
    FetchContent_MakeAvailable(Catch2)
    message(STATUS "Fetched Catch2 version ${Catch2_VERSION}")
else()
    message(STATUS "Using system Catch2 version ${Catch2_VERSION}")
endif()

file(GLOB_RECURSE src_files "*.cpp")

add_executable(tests${module_name} ${src_files})

target_link_libraries(tests${module_name} PUBLIC ${module_name})

target_link_libraries(tests${module_name} PRIVATE Catch2::Catch2WithMain)

# Setup testing
list(APPEND CMAKE_MODULE_PATH ${catch2_SOURCE_DIR}/extras)
include(CTest)
include(Catch)

# Discover and add tests
catch_discover_tests(tests${module_name})

# Set test configuration for CTest
set(CTEST_CONFIGURATION_TYPE ${CMAKE_BUILD_TYPE})