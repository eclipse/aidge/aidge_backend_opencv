/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_OPENCV_TOOLS_H_
#define AIDGE_OPENCV_TOOLS_H_

#include "opencv2/core.hpp"

template <typename T>
cv::Mat createRandomMat(int rows, int cols) {
    cv::Mat randomMat(rows, cols, cv::DataType<T>::type);

    cv::randu(randomMat, cv::Scalar::all(0), cv::Scalar::all(255));

    return randomMat;
}

#endif // AIDGE_OPENCV_TESTS_UTILS_H_