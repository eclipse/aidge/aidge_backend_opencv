/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include "opencv2/core.hpp"
#include <opencv2/imgcodecs.hpp>
#include <memory>
#include <iostream>

#include "aidge/backend/opencv/stimuli/StimulusImpl_opencv_imread.hpp"
#include "aidge/backend/opencv/data/TensorImpl.hpp"
#include "aidge/data/Tensor.hpp"

#include "Tools.hpp"

using namespace Aidge;

TEST_CASE("StimulusImpl_opencv_imread creation", "[StimulusImpl_opencv_imread][OpenCV]") {
    SECTION("Instanciation & load an image") {
        // //  Load image with imread
        // // cv::Mat true_mat = cv::imread("/data1/is156025/tb256203/dev/eclipse_aidge/aidge/user_tests/Lenna.png");
        // cv::Mat true_mat = cv::imread("/data1/is156025/tb256203/dev/eclipse_aidge/aidge/user_tests/train-images-idx3-ubyte[00001].pgm", cv::IMREAD_UNCHANGED);
        // REQUIRE(true_mat.empty()==false);

        // // Create StimulusImpl_opencv_imread
        // // StimulusImpl_opencv_imread stImpl("/data1/is156025/tb256203/dev/eclipse_aidge/aidge/user_tests/Lenna.png");
        // StimulusImpl_opencv_imread stImpl("/data1/is156025/tb256203/dev/eclipse_aidge/aidge/user_tests/train-images-idx3-ubyte[00001].pgm");
        
         //  Generate random matrix and save it
        std::vector<cv::Mat> channels;
        cv::Mat true_mat;

        for (int c = 0; c < 3; ++c){
            // Create a random matrix
            cv::Mat randomMat = createRandomMat<unsigned char>(224, 224);
            // Add each random matrix to the vector
            channels.push_back(randomMat);
        }
        // Merge the vector of cv mat into one cv mat
        cv::merge(channels, true_mat);

        // Save image into a png file
        cv::imwrite("output_image_stimpl.png", true_mat);

        // Instanciate timulusImpl_opencv_imread
        StimulusImpl_opencv_imread stImpl("output_image_stimpl.png");

        // Load the image as a tensor
        std::shared_ptr<Tensor> tensor_load;
        tensor_load = stImpl.load();

        // Access the cv::Mat with the tensor
        TensorImpl_opencv_* tImpl_opencv = dynamic_cast<TensorImpl_opencv_*>(tensor_load->getImpl().get());
        auto mat_tensor = tImpl_opencv->data();

        // Check the dimensions
        REQUIRE((mat_tensor.total() * mat_tensor.channels()) == (true_mat.total() * true_mat.channels()));

        // Split it in channels
        std::vector<cv::Mat> channels_tensor;
        cv::split(mat_tensor, channels_tensor);

        REQUIRE(channels_tensor.size() == channels.size());

        // Check the elements
        for (size_t i = 0; i < channels_tensor.size(); ++i) {
            REQUIRE(cv::countNonZero(channels_tensor[i] != channels[i]) == 0);   
        }
    }
}
