import unittest
import aidge_core
import aidge_backend_opencv
import numpy as np


class test_tensor(unittest.TestCase):
    """Test tensor binding
    """
    def setUp(self):
        pass
    def tearDown(self):
        pass

    def test_getavailable_backends(self):
        self.assertTrue("opencv" in aidge_core.Tensor.get_available_backends())

    # def test_numpy_int_to_tensor(self):
    #     np_array = np.arange(9).reshape(1,1,3,3)
    #     # Numpy -> Tensor
    #     t = aidge_core.Tensor(np_array)
    #     self.assertEqual(t.dtype(), aidge_core.dtype.int32)
    #     for i_t, i_n in zip(t, np_array.flatten()):
    #         self.assertTrue(i_t == i_n)
    #     for i,j in zip(t.dims(), np_array.shape):
    #         self.assertEqual(i,j)

    # def test_tensor_int_to_numpy(self):
    #     np_array = np.arange(9).reshape(1,1,3,3)
    #     # Numpy -> Tensor
    #     t = aidge_core.Tensor(np_array)
    #     # Tensor -> Numpy
    #     nnarray = np.array(t)
    #     for i_nn, i_n in zip(nnarray.flatten(), np_array.flatten()):
    #         self.assertTrue(i_nn == i_n)
    #     for i,j in zip(t.dims(), nnarray.shape):
    #         self.assertEqual(i,j)

    # def test_numpy_float_to_tensor(self):
    #     t = aidge_core.Tensor()
    #     np_array = np.random.rand(1, 1, 3, 3).astype(np.float32)
    #     # Numpy -> Tensor
    #     t = aidge_core.Tensor(np_array)
    #     self.assertEqual(t.dtype(), aidge_core.dtype.float32)
    #     for i_t, i_n in zip(t, np_array.flatten()):
    #         self.assertTrue(i_t == i_n) # TODO : May need to change this to a difference
    #     for i,j in zip(t.dims(), np_array.shape):
    #         self.assertEqual(i,j)

if __name__ == '__main__':
    unittest.main()
