/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_OPENCV_STIMULI_STIMULUSIMPLOPENCVIMREAD_H_
#define AIDGE_OPENCV_STIMULI_STIMULUSIMPLOPENCVIMREAD_H_

#include <string>
#include <memory>

#include <opencv2/imgcodecs.hpp>   // cv::IMREAD_COLOR

#include "aidge/data/Tensor.hpp"
#include "aidge/backend/StimulusImpl.hpp"
#include "aidge/stimuli/Stimulus.hpp"
#include "aidge/backend/opencv/data/TensorImpl.hpp"


namespace Aidge {
class StimulusImpl_opencv_imread : public StimulusImpl {
private:
    /// Stimulus data path
    const std::string mDataPath;
    const int mReadMode;

public:
    StimulusImpl_opencv_imread(const std::string& dataPath="", int readMode=cv::IMREAD_UNCHANGED)
    : mDataPath(dataPath),
      mReadMode(readMode)
    {
        // ctor
    }

    ~StimulusImpl_opencv_imread() noexcept;

public:
    static std::unique_ptr<StimulusImpl_opencv_imread> create(const std::string& dataPath) {
        return std::make_unique<StimulusImpl_opencv_imread>(dataPath);
    }

public:
    std::shared_ptr<Tensor> load() const override;
};

namespace {
static Registrar<Aidge::Stimulus> registrarStimulusImpl_opencv_png(
        {"opencv", "png"}, Aidge::StimulusImpl_opencv_imread::create);
static Registrar<Aidge::Stimulus> registrarStimulusImpl_opencv_pgm(
        {"opencv", "pgm"}, Aidge::StimulusImpl_opencv_imread::create);
}  // namespace

} // namespace Aidge

#endif /* AIDGE_OPENCV_STIMULI_STIMULUSIMPLOPENCVIMREAD_H_ */
