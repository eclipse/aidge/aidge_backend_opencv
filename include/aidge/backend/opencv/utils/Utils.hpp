/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_OPENCV_UTILS_UTILS_H_
#define AIDGE_OPENCV_UTILS_UTILS_H_

#include <opencv2/core/mat.hpp>  // cv::Mat
#include <memory>
#include <tuple>
#include <vector>
#include <cstring>
#include <string> 

#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Instanciate an aidge tensor with backend "opencv" from an opencv matrix
 *
 * @param mat the cv::mat to instanciate the tensor from
 * @return std::shared_ptr<Tensor> aidge tensor
 */
std::shared_ptr<Tensor> tensorOpencv(cv::Mat mat);

/**
 * @brief Copy the data from a source 2D cv::mat to a destination pointer with an offset
 *
 * @tparam CV_T The standard type corresponding to the opencv data type
 * @param mat opencv 2D mat to copy the data from
 * @param data destination pointer
 * @param offset offset an the destination data pointer
 */
template <class CV_T>
void convert(const cv::Mat& mat, void* data, std::size_t offset);


/**
 * @brief Convert a tensor backend opencv into a tensor backend cpu
 *
 * @param tensorOpencv tensor with backend opencv (contains a cv::mat)
 * @return std::shared_ptr<Tensor> tensor backend cpu (contains a std::vector)
 */
std::shared_ptr<Tensor> convertCpu(std::shared_ptr<Aidge::Tensor> tensorOpencv);


}  // namespace

#endif // AIDGE_OPENCV_UTILS_UTILS_H_