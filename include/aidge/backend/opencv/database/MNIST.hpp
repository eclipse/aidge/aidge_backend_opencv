/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_OPENCV_DATABASE_MNIST_H_
#define AIDGE_OPENCV_DATABASE_MNIST_H_

#include <algorithm>  // std::reverse
#include <cstddef>    // std:size_t
#include <cstdint>    // std::uint32_t
#include <string>
#include <tuple>      // std::tuple_size
#include <vector>

#include "aidge/data/Database.hpp"
#include "aidge/stimuli/Stimulus.hpp"
// #include "aidge/graph/GraphView.hpp"
// #include "aidge/scheduler/Scheduler.hpp"
#include "aidge/data/Tensor.hpp"

// #include "aidge/backend/opencv/data/TensorImpl.hpp"

namespace Aidge {

template <class T> void swapEndian(T& obj)
{
    std::uint8_t* memp = reinterpret_cast<unsigned char*>(&obj);
    std::reverse(memp, memp + sizeof(T));
}

inline bool isBigEndian()
{
    const union {
        uint32_t i;
        char c[4];
    } bint = {0x01020304};

    return bint.c[0] == 1;
}


class MNIST : public Database {
public:
    union MagicNumber {
        std::uint32_t value;
        std::uint8_t byte[4];
    };

    enum DataType {
        Unsigned = 0x08,
        Signed = 0x09,
        Short = 0x0B,
        Int = 0x0C,
        Float = 0x0D,
        Double = 0x0E
    };

protected:
    /// Stimulus data path
    const std::string mDataPath;

    // True select the train database, False Select the test database
    bool mTrain;

    // True Load images in memory, False reload at each call
    bool mLoadDataInMemory;

    /// Stimulus data
    // Each index of the vector is one item of the database
    // One item of the MNIST database is the tuple <Image,label>
    // First stimuli of the tuple is a gray scale image stimuli of a writen digit
    // Second stimuli of the tuple is the label associated to the digit : unsigned integer 0-9
    mutable std::vector<std::tuple<Stimulus,Stimulus>> mStimuli;

    /// Data Transformations
    // Data transformations use the GraphView mecanism
    // Transformations are a sequential graph with each operator of the graph being one transformation
    // GraphView mDataTransformations;

    // Scheduler to run the graph of data transformations
    // Scheduler mScheduler;

public:
    MNIST(const std::string& dataPath,
            // const GraphView transformations,
            bool train,
            bool loadDataInMemory = false)
    : Database(),
      mDataPath(dataPath),
    // mDataTransformations(transformations),
      mTrain(train),
      mLoadDataInMemory(loadDataInMemory)
    {
        // Uncompress train database
        if (mTrain) {
            uncompress(mDataPath + "/train-images-idx3-ubyte",
                            dataPath + "/train-labels-idx1-ubyte");
        } else { // Uncompress test database
            uncompress(mDataPath + "/t10k-images-idx3-ubyte",
                            dataPath + "/t10k-labels-idx1-ubyte");
        }
    }

    ~MNIST() noexcept;

public:
    std::vector<std::shared_ptr<Tensor>> getItem(const std::size_t index) const override final;

    inline std::size_t getLen() const noexcept override final {
        return mStimuli.size();
    }

    inline std::size_t getNbModalities() const noexcept override final {
        return std::tuple_size<decltype(mStimuli)::value_type>::value;
    }

private:
    void uncompress(const std::string& dataPath, const std::string& labelPath);
};
}

#endif // AIDGE_OPENCV_DATABASE_MNIST_H_
