/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_OPENCV_DATA_DATAUTILS_H_
#define AIDGE_OPENCV_DATA_DATAUTILS_H_


#include "opencv2/core.hpp"

#include <cstdint>

namespace Aidge {

namespace detail {
template <typename T> struct CV_C1_CPP { static constexpr int value = -1; };
template <> struct CV_C1_CPP<std::int8_t> {
    static constexpr int value = CV_8SC1;
};
template <> struct CV_C1_CPP<std::int16_t> {
    static constexpr int value = CV_16SC1;
};
template <> struct CV_C1_CPP<std::int32_t> {
    static constexpr int value = CV_32SC1;
};
template <> struct CV_C1_CPP<std::uint8_t> {
    static constexpr int value = CV_8UC1;
};
template <> struct CV_C1_CPP<std::uint16_t> {
    static constexpr int value = CV_16UC1;
};
template <> struct CV_C1_CPP<float> {
    static constexpr int value = CV_32FC1;
};
template <> struct CV_C1_CPP<double> {
    static constexpr int value = CV_64FC1;
};
template <typename T> constexpr int CV_C1_CPP_v = CV_C1_CPP<T>::value;
} // namespace detail
} // namespace Aidge

#endif /* AIDGE_OPENCV_DATA_DATAUTILS_H_ */