/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_OPENCV_DATA_TENSORIMPL_H_
#define AIDGE_OPENCV_DATA_TENSORIMPL_H_

#include "opencv2/core.hpp"

#include "aidge/backend/TensorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/future_std/span.hpp"
#include "aidge/backend/opencv/data/DataUtils.hpp"


namespace Aidge {

class TensorImpl_opencv_ {
public:
    virtual const cv::Mat& data() const = 0;
    virtual void setCvMat(const cv::Mat& mat ) = 0;
};

template <class T>
class TensorImpl_opencv : public TensorImpl, public TensorImpl_opencv_ {
private:
    // Stores the cv::Mat
    cv::Mat mData;

protected:
    std::vector<DimSize_t> mDims;

public:
    static constexpr const char *Backend = "opencv";

    TensorImpl_opencv() = delete;
    TensorImpl_opencv(DeviceIdx_t device, std::vector<DimSize_t> dims)
    : TensorImpl(Backend, device, dims)
    {
        mDims = dims;
    }

    bool operator==(const TensorImpl &otherImpl) const override final {
        // Create iterators for both matrices
        cv::MatConstIterator_<T> it1 = mData.begin<T>();

        const cv::Mat & otherData = reinterpret_cast<const TensorImpl_opencv<T> &>(otherImpl).data();
        cv::MatConstIterator_<T> it2 = otherData.begin<T>();

        // Iterate over the elements and compare them
        for (; it1 != mData.end<T>(); ++it1, ++it2) {
            if (*it1 != *it2) {
                return false;
            }
        }
        return true;
    }

    static std::unique_ptr<TensorImpl_opencv> create(DeviceIdx_t device, std::vector<DimSize_t> dims) {
        return std::make_unique<TensorImpl_opencv<T>>(device, dims);
    }

    void resize(std::vector<DimSize_t> dims) override{
        mDims = dims;
        size_t product = 1;
        for (size_t num : dims) {
            product *= num;
        }
        mNbElts = product;
    }

    // native interface
    const cv::Mat & data() const override { return mData; }

    inline std::size_t capacity() const noexcept override { return (mData.total() * mData.channels()); }

    inline std::size_t scalarSize() const noexcept override final { return sizeof(T); }

    void zeros() override final {
        mData.setTo(cv::Scalar::all(T(0)));
    }

    void copy(const void *src, NbElts_t length, NbElts_t offset = 0) override final {
        AIDGE_ASSERT(offset + length <= mNbElts, "TensorImpl_opencv<{}>::copy(): copy offset ({}) + length ({}) is above capacity ({})", typeid(T).name(), offset, length, mNbElts);
        const T* srcT = static_cast<const T *>(src);
        T* dstT = static_cast<T *>(rawPtr(offset));

        AIDGE_ASSERT(dstT < srcT || dstT >= srcT + length, "TensorImpl_opencv<{}>::copy(): overlapping copy is not supported", typeid(T).name());
        std::copy(srcT, srcT + length, dstT);

    }

    void copyCast(const void *src, const DataType srcDt, NbElts_t length, NbElts_t offset = 0) override final{
        if (length == 0) {
            return;
        }

        AIDGE_ASSERT(offset + length <= mNbElts, "TensorImpl_opencv<{}>::copyCast(): copy offset ({}) + length ({}) is above capacity ({})", typeid(T).name(), offset, length, mNbElts);
        T* dstT = static_cast<T *>(rawPtr(offset));
        switch (srcDt)
        {
            case DataType::Float64:
                std::copy(static_cast<const double*>(src), static_cast<const double*>(src) + length,
                        dstT);
                break;
            case DataType::Float32:
                std::copy(static_cast<const float*>(src), static_cast<const float*>(src) + length,
                        dstT);
                break;
            case DataType::Float16:
                std::copy(static_cast<const half_float::half*>(src), static_cast<const half_float::half*>(src) + length,
                        dstT);
                break;
            case DataType::Int64:
                std::copy(static_cast<const int64_t*>(src), static_cast<const int64_t*>(src) + length,
                        dstT);
                break;
            case DataType::UInt64:
                std::copy(static_cast<const uint64_t*>(src), static_cast<const uint64_t*>(src) + length,
                        dstT);
                break;
            case DataType::Int32:
                std::copy(static_cast<const int32_t*>(src), static_cast<const int32_t*>(src) + length,
                        dstT);
                break;
            case DataType::UInt32:
                std::copy(static_cast<const uint32_t*>(src), static_cast<const uint32_t*>(src) + length,
                        dstT);
                break;
            case DataType::Int16:
                std::copy(static_cast<const int16_t*>(src), static_cast<const int16_t*>(src) + length,
                        dstT);
                break;
            case DataType::UInt16:
                std::copy(static_cast<const uint16_t*>(src), static_cast<const uint16_t*>(src) + length,
                        dstT);
                break;
            case DataType::Int8:
                std::copy(static_cast<const int8_t*>(src), static_cast<const int8_t*>(src) + length,
                        dstT);
                break;
            case DataType::UInt8:
                std::copy(static_cast<const uint8_t*>(src), static_cast<const uint8_t*>(src) + length,
                        dstT);
                break;
            default:
                AIDGE_THROW_OR_ABORT(std::runtime_error, "TensorImpl_opencv<{}>::copyCast(): unsupported data type {}.", typeid(T).name(), srcDt);
                break;
        }
    }


    void copyFromDevice(const void *src, const std::pair<std::string, DeviceIdx_t>& device, NbElts_t length, NbElts_t offset = 0) override final {
        AIDGE_ASSERT(device.first == Backend, "TensorImpl_opencv<{}>::copyFromDevice(): backend must match", typeid(T).name());
        AIDGE_ASSERT(device.second == 0, "TensorImpl_opencv<{}>::copyFromDevice(): device ({}) cannot be != 0 for CPU backend", typeid(T).name(), device.second);
        copy(src, length, offset);
    }

    void copyFromHost(const void *src, NbElts_t length, NbElts_t offset = 0) override final {
        copy(src, length, offset);
    }

    void copyToHost(void *dst, NbElts_t length, NbElts_t offset = 0) const override final {
        AIDGE_ASSERT(offset + length <= mNbElts, "TensorImpl_opencv<{}>::copyToHost(): copy offset ({}) + length ({}) is above capacity ({})", typeid(T).name(), offset, length, mNbElts);
        const T* src = static_cast<const T*>(rawPtr(offset));
        std::copy(src, src + length, static_cast<T *>(dst));
    }

    void *rawPtr(NbElts_t offset = 0) override final {
        lazyInit();
        return (mData.ptr<T>() + offset);
    };

    const void *rawPtr(NbElts_t offset = 0) const override final {
        AIDGE_ASSERT((mData.total() * mData.channels()) >= mNbElts, "TensorImpl_opencv<{}>::rawPtr(): accessing uninitialized const rawPtr", typeid(T).name());
        return (mData.ptr<T>() + offset);
    };

    void *hostPtr(NbElts_t offset = 0) override final {
        lazyInit();
        return (mData.ptr<T>() + offset);
    };

    const void *hostPtr(NbElts_t offset = 0) const override {
        AIDGE_ASSERT((mData.total() * mData.channels()) >= mNbElts, "TensorImpl_opencv<{}>::hostPtr(): accessing uninitialized const hostPtr", typeid(T).name());
        AIDGE_ASSERT(mData.isContinuous(), "TensorImpl_opencv<{}>::hostPtr(): CV Matrix not continuous", typeid(T).name());
        return (mData.ptr<T>() + offset);
    };

    void setCvMat(const cv::Mat& mat) override {mData=mat;}


  virtual ~TensorImpl_opencv() = default;

private:

  void lazyInit() {
    if ((mData.total() * mData.channels()) < mNbElts) {
        // Need more data, a re-allocation will occur
        AIDGE_ASSERT(mData.empty(), "TensorImpl_opencv<{}>: trying to enlarge non-owned data", typeid(T).name());

        if (mDims.size() < 3) {
            mData = cv::Mat(((mDims.size() > 1) ? static_cast<int>(mDims[0])
                                                    : (mDims.size() > 0) ? 1
                                                                            : 0),
                            (mDims.size() > 0) ? static_cast<int>(mDims[1]) : 0,
                            detail::CV_C1_CPP_v<T>);
        } else {
            std::vector<cv::Mat> channels;

            for (std::size_t k = 0; k < mDims[2]; ++k) {
                channels.push_back(cv::Mat(static_cast<int>(mDims[0]),
                                        static_cast<int>(mDims[1]),
                                        detail::CV_C1_CPP_v<T>));
            }

            cv::merge(channels, mData);
        }
    }
  }
};

namespace {
static Registrar<Tensor> registrarTensorImpl_opencv_Float64(
        {"opencv", DataType::Float64}, Aidge::TensorImpl_opencv<double>::create);
static Registrar<Tensor> registrarTensorImpl_opencv_Float32(
        {"opencv", DataType::Float32}, Aidge::TensorImpl_opencv<float>::create);
static Registrar<Tensor> registrarTensorImpl_opencv_Int32(
        {"opencv", DataType::Int32}, Aidge::TensorImpl_opencv<std::int32_t>::create);
static Registrar<Tensor> registrarTensorImpl_opencv_Int16(
        {"opencv", DataType::Int16}, Aidge::TensorImpl_opencv<std::int16_t>::create);
static Registrar<Tensor> registrarTensorImpl_opencv_UInt16(
        {"opencv", DataType::UInt16}, Aidge::TensorImpl_opencv<std::uint16_t>::create);
static Registrar<Tensor> registrarTensorImpl_opencv_Int8(
        {"opencv", DataType::Int8}, Aidge::TensorImpl_opencv<std::int8_t>::create);
static Registrar<Tensor> registrarTensorImpl_opencv_UInt8(
        {"opencv", DataType::UInt8}, Aidge::TensorImpl_opencv<std::uint8_t>::create);
} // namespace
} // namespace Aidge

#endif /* AIDGE_OPENCV_DATA_TENSORIMPL_H_ */
